(define-module (raylib)
  #:use-module (scheme inexact)
  #:use-module (srfi srfi-9)
  #:use-module (system foreign)
  #:use-module (system foreign-library)
  ;; core  ===================================
  #:export (
    ;; window-related functions
    init-window
    close-window 
    window-should-close
    is-window-ready
    ;; drawing-related functions
    begin-drawing
    end-drawing
    clear-background
    begin-mode-2d
    end-mode-2d
    ;; timing-related functions
    set-target-fps
    ;; input-related functions: keyboard 
    is-key-pressed
    is-key-down
    is-key-released
    is-key-up
    set-exit-key
    get-key-pressed
    get-char-pressed
    ;; input-related functions: mouse 
    is-mouse-button-pressed
    is-mouse-button-down
    is-mouse-button-released
    get-mouse-x
    get-mouse-y
    get-mouse-position
    set-mouse-position
    set-mouset-offset
    set-mouse-scale
    get-mouse-wheel-move
    set-mouse-cursor
    ;; misc functions
    take-screenshot
    set-trace-log-level)

  ;; shapes ==================================
  #:export (
    ;; basic shapes drawing functions
    draw-pixel
    draw-circle-v
    draw-circle-lines
    draw-line-ex
    draw-rectangle
    draw-rectangle-rec
    draw-rectangle-lines)
  ;; textures ================================
  #:export (
  
    load-image
    gen-image-color
    image-draw-pixel
    unload-image
    load-texture-from-image
    unload-texture
    draw-texture

    fade
    get-pixel-data-size 

    texture?
    image?
    image-width
    image-height
    image-format
    image-data
  )
  ;; text ====================================
  #:export (
    ;; text drawing functions
    draw-fps
    draw-text
    ;; text misc. functions
    measure-text)

  ;; helpers =================================
  #:export (number->int)
  ;; structs =================================
  #:export (
    ;; color 
    make-color 
    color? 
    color-r 
    color-g 
    color-b 
    color-a
    ;; rectangle 
    make-rectangle 
    rectangle?
    rectangle-x rectangle-x!
    rectangle-y rectangle-y!
    rectangle-w rectangle-w!
    rectangle-h rectangle-h!
    rectangle-w rectangle-w!

    ;; camera2d 
    make-camera2d     
    camera?
    camera2d-offset   camera2d-offset! 
    camera2d-target   camera2d-target!
    camera2d-rotation camera2d-rotation!
    camera2d-zoom     camera2d-zoom!) 

  ;; colors ==================================
  #:export (
    LIGHTGRAY   GRAY         DARKGRAY   YELLOW
    GOLD        ORANGE       PINK       RED
    MAROON      GREEN        LIME       DARKGREEN
    SKYBLUE     BLUE         DARKBLUE   PURPLE
    VIOLET      DARKPURPLE   BEIGE      BROWN
    DARKBROWN   WHITE        BLACK      BLANK
    MAGENTA     RAYWHITE)

  ;; log verbosity =====================================
  #:export (
    LOG-ALL       LOG-TRACE
    LOG-DEBUG     LOG-INFO
    LOG-WARNING   LOG-ERROR
    LOG-FATAL     LOG-NONE)

  ;; Mouse 
  #:export (
    MOUSE-BUTTON-LEFT MOUSE-BUTTON-RIGHT  MOUSE-BUTTON-MIDDLE
    MOUSE-BUTTON-SIDE MOUSE-BUTTON-EXTRA  MOUSE-BUTTON-FORWARD
    MOUSE-BUTTON-BACK)

  ;; Mouse cursor
  #:export (
    MOUSE-CURSOR-DEFAULT       MOUSE-CURSOR-ARROW         MOUSE-CURSOR-IBEAM         
    MOUSE-CURSOR-CROSSHAIR     MOUSE-CURSOR-POINTING-HAND MOUSE-CURSOR-RESIZE-EW     
    MOUSE-CURSOR-RESIZE_NS     MOUSE-CURSOR-RESIZE-NWSE   MOUSE-CURSOR-RESIZE-NESW   
    MOUSE-CURSOR-RESIZE_ALL    MOUSE-CURSOR-NOT-ALLOWED )

  ;; keys  ========================================
  #:export (
    KEY-NULL
    ;; alphanumeric keys 
    KEY-APOSTROPHE     KEY-COMMA           KEY-MINUS        KEY-PERIOD         KEY-SLASH         KEY-ZERO
    KEY-ONE            KEY-TWO             KEY-THREE        KEY-FOUR           KEY-FIVE          KEY-SIX
    KEY-SEVEN          KEY-EIGHT           KEY-NINE         KEY-SEMICOLON      KEY-EQUAL         KEY-A
    KEY-B              KEY-C               KEY-D            KEY-E              KEY-F             KEY-G
    KEY-H              KEY-I               KEY-J            KEY-K              KEY-L             KEY-M
    KEY-N              KEY-O               KEY-P            KEY-Q              KEY-R             KEY-S
    KEY-T              KEY-U               KEY-V            KEY-W              KEY-X             KEY-Y
    KEY-Z
    ;; function keys 
    KEY-SPACE          KEY-ESCAPE          KEY-ENTER        KEY-TAB            KEY-BACKSPACE     KEY-INSERT
    KEY-DELETE         KEY-RIGHT           KEY-LEFT         KEY-DOWN           KEY-UP            KEY-PAGE-UP
    KEY-PAGE-DOWN      KEY-HOME            KEY-END          KEY-CAPS-LOCK      KEY-SCROLL-LOCK   KEY-NUM-LOCK
    KEY-PRINT-SCREEN   KEY-PAUSE           KEY-F1           KEY-F2             KEY-F3            KEY-F4
    KEY-F5             KEY-F6              KEY-F7           KEY-F8             KEY-F9            KEY-F10
    KEY-F11            KEY-F12             KEY-LEFT-SHIFT   KEY-LEFT-CONTROL   KEY-LEFT-ALT      KEY-LEFT-SUPER
    KEY-RIGHT-SHIFT    KEY-RIGHT-CONTROL   KEY-RIGHT-ALT    KEY-RIGHT-SUPER    KEY-KB-MENU       KEY-LEFT-BRACKET
    KEY-BACKSLASH      KEY-RIGHT-BRACKET   KEY-GRAVE
    ;; keypad keys 
    KEY-KP-0           KEY-KP-1            KEY-KP-2         KEY-KP-3           KEY-KP-4          KEY-KP-5
    KEY-KP-6           KEY-KP-7            KEY-KP-8         KEY-KP-9           KEY-KP-DECIMAL    KEY-KP-DIVIDE
    KEY-KP-MULTIPLY    KEY-KP-SUBTRACT     KEY-KP-ADD       KEY-KP-ENTER       KEY-KP-EQUAL
    ;; android key buttons
    KEY-BACK           KEY-MENU            KEY-VOLUME-UP    KEY-VOLUME-DOWN))

;; helpers ====================================================================

(define (number->int n) (exact (round n)))
(define (int->bool i) (equal? 1 i))

;; structs ====================================================================

(define-record-type <color>
  (make-color r g b a)
  color? 
  (r color-r) (g color-g) (b color-b) (a color-a))
(define (color->list color)
  (list (color-r color) (color-g color) (color-b color) (color-a color)))

(define color-type (list uint8 uint8 uint8 uint8))
(define (color->c-struct color) 
  (make-c-struct color-type (color->list color)))
(define (parse-color-struct  struct)
  (apply make-color (parse-c-struct struct color-type)))


(define-record-type <rectangle> 
  (make-rectangle x y w h) 
  rectangle? 
  (x rectangle-x rectangle-x!)
  (y rectangle-y rectangle-y!)
  (w rectangle-w rectangle-w!)
  (h rectangle-h rectangle-h!)) 
(define rectangle-type (list float float float float))
(define (rectangle->c-struct rec) 
  (make-c-struct rectangle-type (rectangle->list rec))) 
(define (rectangle->list rec) 
  (list 
      (inexact (rectangle-x rec)) 
      (inexact (rectangle-y rec)) 
      (inexact (rectangle-w rec))
      (inexact (rectangle-h rec))))


(define vector2-type (list float float))
(define (vector2->list vec2) 
  (list 
    (inexact (vector-ref vec2 0))
    (inexact (vector-ref vec2 1))))
(define (vec2-struct->vector vec2)
  (list->vector (parse-c-struct vec2 vector2-type)))
(define (vector2->c-struct vec)
  (make-c-struct vector2-type (vector2->list vec)))


(define-record-type <camera2d> 
  (make-camera2d offset target rotation zoom) 
  camera2d?
  (offset   camera2d-offset   camera2d-offset!) 
  (target   camera2d-target   camera2d-target!)
  (rotation camera2d-rotation camera2d-rotation!)
  (zoom     camera2d-zoom     camera2d-zoom!)) 
(define (camera2d->list camera)
  (list 
    (vector2->list (camera2d-offset camera))
    (vector2->list (camera2d-target camera))
    (inexact (camera2d-rotation camera))
    (inexact (camera2d-zoom camera))))
(define camera2d-type (list vector2-type vector2-type float float))
(define (camera2d->c-struct camera)
  (make-c-struct camera2d-type (camera2d->list camera)))

(define-record-type <image>
  (make-image data width height mipmaps format)
  image?
  (data    image-data    image-data!)
  (width   image-width   image-width!)
  (height  image-height  image-height!)
  (mipmaps image-mipmaps image-mipmaps!)
  (format  image-format  image-format!))
(define (image->list image)
  (list 
    (bytevector->pointer (image-data image))
    ;; (image-data image)
    (number->int (image-width  image))
    (number->int (image-height image))
    (number->int (image-mipmaps image))
    (number->int (image-format image))))
(define image-type (list '* int int int int))
(define (image->c-struct image)
  (make-c-struct image-type (image->list image)))
(define (image-struct->image struct)
  (let ((img (apply make-image (parse-c-struct struct image-type))))
    (image-data! img 
      (pointer->bytevector 
        (image-data img) 
        (get-pixel-data-size (image-width img) (image-height img) (image-format img))))
    img))

(define-record-type <texture>
  (make-texture id width height mipmaps format)
  texture?
  (id      texture-id texture-id!)
  (width   texture-width   texture-width!)
  (height  texture-height  texture-height!)
  (mipmaps texture-mipmaps texture-mipmaps!)
  (format  texture-format  texture-format!))
(define (texture->list texture)
  (list 
    (number->int (texture-id texture))
    (number->int (texture-width  texture))
    (number->int (texture-height texture))
    (number->int (texture-mipmaps texture))
    (number->int (texture-format texture))))
(define texture-type (list int int int int int))
(define (texture->c-struct texture)
  (make-c-struct texture-type (texture->list texture)))
(define (texture-struct->texture struct)
  (apply make-texture (parse-c-struct struct texture-type)))



;; core =======================================================================

;; window-related functions ---------------------------------------------------
(define init-window
  (let 
    ((wf (foreign-library-function "libraylib.so" "InitWindow"
      #:return-type void #:arg-types (list int int '*))))
    (lambda (w h title) (wf w h (string->pointer title)))))


(define window-should-close
  (let 
    ((wf (foreign-library-function "libraylib.so" "WindowShouldClose"
      #:return-type int)))
    (lambda () (int->bool (wf)))))

(define is-window-ready 
  (let 
    ((wf (foreign-library-function "libraylib.so" "IsWindowReady"
      #:return-type int)))
    (lambda () (int->bool (wf)))))

(define close-window
 (foreign-library-function "libraylib.so" "CloseWindow"
  #:return-type void))

(define clear-background
  (let 
    ((wf (foreign-library-function "libraylib.so" "ClearBackground"
      #:return-type void #:arg-types (list (list uint8 uint8 uint8 uint8)))))
    (lambda (color) (wf (color->c-struct color)))))

;; drawing-related functions --------------------------------------------------

(define begin-drawing
  (foreign-library-function "libraylib.so" "BeginDrawing"
  #:return-type void))

(define end-drawing
  (foreign-library-function "libraylib.so" "EndDrawing"
  #:return-type void))

(define begin-mode-2d
  (let 
    ((wf (foreign-library-function "libraylib.so" "BeginMode2D"
      #:return-type void #:arg-types (list camera2d-type))))
    (lambda (camera)
      (wf (camera2d->c-struct camera)))))

(define end-mode-2d
 (foreign-library-function "libraylib.so" "EndMode2D"))


;; timing-related functions ---------------------------------------------------

(define set-target-fps 
  (foreign-library-function "libraylib.so" "SetTargetFPS"
  #:return-type void #:arg-types (list int)))

;; input-related functions: keyboard ------------------------------------------

(define is-key-pressed
  (let ((wf (foreign-library-function "libraylib.so" "IsKeyPressed"
      #:return-type int8 #:arg-types (list int))))
    (lambda (key) (int->bool (wf key)))))

(define is-key-down
  (let 
    ((wf (foreign-library-function "libraylib.so" "IsKeyDown"
      #:return-type int8 #:arg-types (list int))))
    (lambda (key) (int->bool (wf key)))))

(define is-key-released
  (let 
    ((wf (foreign-library-function "libraylib.so" "IsKeyReleased"
      #:return-type int8 #:arg-types (list int))))
    (lambda (key) (int->bool (wf key)))))

(define is-key-up
  (let 
    ((wf (foreign-library-function "libraylib.so" "IsKeyUp"
      #:return-type int8 #:arg-types (list int))))
    (lambda (key) (int->bool (wf key)))))

(define set-exit-key
  (let 
    ((wf (foreign-library-function "libraylib.so" "SetExitKey"
      #:return-type void #:arg-types (list int))))
    (lambda (key) (wf key))))

(define get-key-pressed
  (foreign-library-function "libraylib.so" "GetKeyPressed"
    #:return-type int #:arg-types (list)))

(define get-char-pressed
  (foreign-library-function "libraylib.so" "GetCharPressed"
    #:return-type int #:arg-types (list)))


;; input-related functions: mouse  --------------------------------------------

(define is-mouse-button-pressed 
  (let 
    ((wf (foreign-library-function "libraylib.so" "IsMouseButtonPressed" 
      #:return-type int8
      #:arg-types (list int))))
    (lambda (button) (int->bool (wf button)))))

(define is-mouse-button-down 
  (let 
    ((wf (foreign-library-function "libraylib.so" "IsMouseButtonDown"
      #:return-type int8
      #:arg-types (list int))))
    (lambda (button) 
      (int->bool (wf button)))))

(define is-mouse-button-released 
  (let 
    ((wf (foreign-library-function "libraylib.so" "IsMouseButtonReleased"
      #:return-type int8
      #:arg-types (list int))))
    (lambda (button) (int->bool (wf button)))))

(define is-mouse-button-up
  (let ((wf (foreign-library-function "libraylib.so" "IsMouseButtonUp"
    #:return-type int8
    #:arg-types (list int))))
    (lambda (button) (int->bool (wf button)))))

(define get-mouse-x 
  (foreign-library-function "libraylib.so" "GetMouseX"
    #:return-type int
    #:arg-types (list)))

(define get-mouse-y 
  (foreign-library-function "libraylib.so" "GetMouseY"
    #:return-type int
    #:arg-types (list)))

(define get-mouse-position
  (lambda () (vector (get-mouse-x) (get-mouse-y))))

(define set-mouse-position
  (foreign-library-function "libraylib.so" "SetMousePosition"
    #:return-type void #:arg-types (list int int)))
 
(define set-mouse-offset
  (foreign-library-function "libraylib.so" "SetMouseOffset"
    #:return-type void #:arg-types (list int int)))

(define set-mouse-scale
  (foreign-library-function "libraylib.so" "SetMouseScale"
    #:return-type void #:arg-types (list float float)))

(define get-mouse-wheel-move
  (foreign-library-function "libraylib.so" "GetMouseWheelMove"
    #:return-type float #:arg-types (list)))

(define set-mouse-cursor
  (foreign-library-function "libraylib.so" "SetMouseCursor"
    #:return-type void #:arg-types (list int)))

;; misc functions -------------------------------------------------------------

(define take-screenshot 
  (let 
    ((wf (foreign-library-function "libraylib.so" "TakeScreenshot"
      #:return-type void #:arg-types (list '*))))
    (lambda (filename) (wf (string->pointer filename)))))

(define set-trace-log-level
  (foreign-library-function "libraylib.so" "SetTraceLogLevel"
    #:return-type void
    #:arg-types (list int)))

;; shapes =====================================================================

(define draw-pixel
  (let 
    ((wf (foreign-library-function "libraylib.so" "DrawPixel"
      #:return-type void
      #:arg-types (list int int color-type))))
    (lambda (x y color) 
      (wf x y (color->c-struct color)))) )
  
(define draw-circle-v 
  (let 
    ((wf (foreign-library-function "libraylib.so" "DrawCircleV"
      #:return-type void #:arg-types (list vector2-type float color-type))))
    (lambda (posvec radius color)
      (wf (vector2->c-struct posvec) radius (color->c-struct color)))))

(define draw-circle-lines
   (let 
    ((wf (foreign-library-function "libraylib.so" "DrawCircleLines"
      #:return-type void #:arg-types (list int int float color-type))))
    (lambda (x y radius color)
      (wf x y radius (color->c-struct color)))))
 

(define draw-line-ex 
  (let
    ((wf (foreign-library-function "libraylib.so" "DrawLineEx"
      #:return-type void #:arg-types (list vector2-type vector2-type float color-type))))
    (lambda (start-pos end-pos thick color)
      (wf (vector2->c-struct start-pos) (vector2->c-struct end-pos) thick (color->c-struct color)))))

(define draw-rectangle 
  (let 
    ((wf (foreign-library-function "libraylib.so" "DrawRectangle"
      #:return-type void #:arg-types (list int int int int color-type))))
    (lambda (x y w h color)
      (wf x y w h (color->c-struct color)))))

(define draw-rectangle-rec
  (let 
    ((wf (foreign-library-function "libraylib.so" "DrawRectangleRec"
      #:return-type void #:arg-types (list rectangle-type color-type))))
    (lambda (rec color)
      (wf (rectangle->c-struct rec) (color->c-struct color)))))

(define draw-rectangle-lines
  (let 
    ((wf (foreign-library-function "libraylib.so" "DrawRectangleLines"
      #:return-type void #:arg-types (list int int int int color-type))))
    (lambda (x y w h color)
      (wf x y w h (color->c-struct color)))))

;; textures ===================================================================

(define fade 
  (let 
    ((wf (foreign-library-function "libraylib.so" "Fade"
      #:return-type color-type #:arg-types (list color-type float))))
    (lambda (color alpha)
      (parse-color-struct 
        (wf (color->c-struct color) (inexact alpha))))))

(define get-pixel-data-size 
  (let 
    ((wf (foreign-library-function "libraylib.so" "GetPixelDataSize"
      #:return-type int #:arg-types (list int int int))))
    (lambda (width height format)
      (wf (number->int width) (number->int height) (number->int format)))))

(define load-image
  (let
    ((wf (foreign-library-function "libraylib.so" "LoadImage"
      #:return-type image-type #:arg-types (list '*))))
    (lambda (file-name)
      (image-struct->image (wf (string->pointer file-name))))))

(define image-draw-pixel 
  (let 
    ((wf (foreign-library-function "libraylib.so" "ImageDrawPixel"
      #:return-type void #:arg-types (list '* int int color-type)))) 
  (lambda (img x y color)
    (wf (image->c-struct img) x y (color->c-struct color)))))

(define unload-image 
  (let
    ((wf (foreign-library-function "libraylib.so" "UnloadImage"
      #:return-type void #:arg-types (list image-type))))
    (lambda (img) (wf (image->c-struct img)))))

(define load-texture-from-image
  (let 
    ((wf (foreign-library-function "libraylib.so" "LoadTextureFromImage"
      #:return-type texture-type #:arg-types (list image-type))))
    (lambda (img)
      (texture-struct->texture (wf (image->c-struct img))))))

(define gen-image-color
  (let 
    ((wf (foreign-library-function "libraylib.so" "GenImageColor"
      #:return-type image-type #:arg-types (list int int color-type))))
    (lambda (width height color)
      (image-struct->image (wf (number->int width) (number->int height) (color->c-struct color))))))
      

(define unload-texture
  (let
    ((wf (foreign-library-function "libraylib.so" "UnloadTexture"
      #:return-type void #:arg-types (list texture-type))))
    (lambda (texture) (wf (texture->c-struct texture)))))

(define draw-texture 
  (let 
    ((wf (foreign-library-function "libraylib.so" "DrawTexture"
      #:return-type void  #:arg-types (list texture-type int int color-type))))
    (lambda (texture x y tint)
      (wf (texture->c-struct texture) (number->int x) (number->int y) (color->c-struct tint)))))

;; text =======================================================================

;; text drawing functions 
(define draw-fps
  (let
    ((wf (foreign-library-function "libraylib.so" "DrawFPS"
      #:return-type void #:arg-types (list int int))))
    (lambda (x y) (wf x y))))

(define draw-text 
  (let 
    ((wf (foreign-library-function "libraylib.so" "DrawText"
      #:return-type void #:arg-types (list '* int int int color-type))))
    (lambda (text x y font-size color) 
      (wf (string->pointer text) x y font-size (color->c-struct color)))))


;; text misc. 
(define measure-text
  (let 
    ((wf (foreign-library-function "libraylib.so" "MeasureText"
      #:return-type int #:arg-types (list '* int))))
    (lambda (text font-size)
      (wf (string->pointer text) font-size))))

;; constants ==================================================================

;; colors     
(define LIGHTGRAY  (make-color 200 200 200 255))
(define GRAY       (make-color 130 130 130 255))
(define DARKGRAY   (make-color 80  80  80  255))
(define YELLOW     (make-color 253 249 0   255))
(define GOLD       (make-color 255 203 0   255))
(define ORANGE     (make-color 255 161 0   255))
(define PINK       (make-color 255 109 194 255))
(define RED        (make-color 230 41  55  255))
(define MAROON     (make-color 190 33  55  255))
(define GREEN      (make-color 0   228 48  255))
(define LIME       (make-color 0   158 47  255))
(define DARKGREEN  (make-color 0   117 44  255))
(define SKYBLUE    (make-color 102 191 255 255))
(define BLUE       (make-color 0   121 241 255))
(define DARKBLUE   (make-color 0   82  172 255))
(define PURPLE     (make-color 200 122 255 255))
(define VIOLET     (make-color 135 60  190 255))
(define DARKPURPLE (make-color 112 31  126 255))
(define BEIGE      (make-color 211 176 131 255))
(define BROWN      (make-color 127 106 79  255))
(define DARKBROWN  (make-color 76  63  47  255))
(define WHITE      (make-color 255 255 255 255))
(define BLACK      (make-color 0   0   0   255))
(define BLANK      (make-color 0   0   0   0  ))
(define MAGENTA    (make-color 255 0   255 255))
(define RAYWHITE   (make-color 245 245 245 255))

;; mouse buttons 
(define MOUSE-BUTTON-LEFT      0) ;; Mouse button left
(define MOUSE-BUTTON-RIGHT     1) ;; Mouse button right
(define MOUSE-BUTTON-MIDDLE    2) ;; Mouse button middle (pressed wheel)
(define MOUSE-BUTTON-SIDE      3) ;; Mouse button side (advanced mouse device)
(define MOUSE-BUTTON-EXTRA     4) ;; Mouse button extra (advanced mouse device)
(define MOUSE-BUTTON-FORWARD   5) ;; Mouse button fordward (advanced mouse device)
(define MOUSE-BUTTON-BACK      6) ;; Mouse button back (advanced mouse device)


;; keyboard keys 
(define KEY-NULL          0)
;; alphanumeric keys
(define KEY-APOSTROPHE    39)
(define KEY-COMMA         44)
(define KEY-MINUS         45)
(define KEY-PERIOD        46)
(define KEY-SLASH         47)
(define KEY-ZERO          48)
(define KEY-ONE           49)
(define KEY-TWO           50)
(define KEY-THREE         51)
(define KEY-FOUR          52)
(define KEY-FIVE          53)
(define KEY-SIX           54)
(define KEY-SEVEN         55)
(define KEY-EIGHT         56)
(define KEY-NINE          57)
(define KEY-SEMICOLON     59)
(define KEY-EQUAL         61)
(define KEY-A             65)
(define KEY-B             66)
(define KEY-C             67)
(define KEY-D             68)
(define KEY-E             69)
(define KEY-F             70)
(define KEY-G             71)
(define KEY-H             72)
(define KEY-I             73)
(define KEY-J             74)
(define KEY-K             75)
(define KEY-L             76)
(define KEY-M             77)
(define KEY-N             78)
(define KEY-O             79)
(define KEY-P             80)
(define KEY-Q             81)
(define KEY-R             82)
(define KEY-S             83)
(define KEY-T             84)
(define KEY-U             85)
(define KEY-V             86)
(define KEY-W             87)
(define KEY-X             88)
(define KEY-Y             89)
(define KEY-Z             90)
;; function keys
(define KEY-SPACE         32)
(define KEY-ESCAPE        256)
(define KEY-ENTER         257)
(define KEY-TAB           258)
(define KEY-BACKSPACE     259)
(define KEY-INSERT        260)
(define KEY-DELETE        261)
(define KEY-RIGHT         262)
(define KEY-LEFT          263)
(define KEY-DOWN          264)
(define KEY-UP            265)
(define KEY-PAGE-UP       266)
(define KEY-PAGE-DOWN     267)
(define KEY-HOME          268)
(define KEY-END           269)
(define KEY-CAPS-LOCK     280)
(define KEY-SCROLL-LOCK   281)
(define KEY-NUM-LOCK      282)
(define KEY-PRINT-SCREEN  283)
(define KEY-PAUSE         284)
(define KEY-F1            290)
(define KEY-F2            291)
(define KEY-F3            292)
(define KEY-F4            293)
(define KEY-F5            294)
(define KEY-F6            295)
(define KEY-F7            296)
(define KEY-F8            297)
(define KEY-F9            298)
(define KEY-F10           299)
(define KEY-F11           300)
(define KEY-F12           301)
(define KEY-LEFT-SHIFT    340)
(define KEY-LEFT-CONTROL  341)
(define KEY-LEFT-ALT      342)
(define KEY-LEFT-SUPER    343)
(define KEY-RIGHT-SHIFT   344)
(define KEY-RIGHT-CONTROL 345)
(define KEY-RIGHT-ALT     346)
(define KEY-RIGHT-SUPER   347)
(define KEY-KB-MENU       348)
(define KEY-LEFT-BRACKET  91)
(define KEY-BACKSLASH     92)
(define KEY-RIGHT-BRACKET 93)
(define KEY-GRAVE         96)
;; keypad keys
(define KEY-KP-0          320)
(define KEY-KP-1          321)
(define KEY-KP-2          322)
(define KEY-KP-3          323)
(define KEY-KP-4          324)
(define KEY-KP-5          325)
(define KEY-KP-6          326)
(define KEY-KP-7          327)
(define KEY-KP-8          328)
(define KEY-KP-9          329)
(define KEY-KP-DECIMAL    330)
(define KEY-KP-DIVIDE     331)
(define KEY-KP-MULTIPLY   332)
(define KEY-KP-SUBTRACT   333)
(define KEY-KP-ADD        334)
(define KEY-KP-ENTER      335)
(define KEY-KP-EQUAL      336)
;; android key buttons
(define KEY-BACK          4)
(define KEY-MENU          82)
(define KEY-VOLUME-UP     24)
(define KEY-VOLUME-DOWN   25)

;; Mouse cursor
(define MOUSE-CURSOR-DEFAULT        0)  ;; Default pointer shape
(define MOUSE-CURSOR-ARROW          1)  ;; Arrow shape
(define MOUSE-CURSOR-IBEAM          2)  ;; Text writing cursor shape
(define MOUSE-CURSOR-CROSSHAIR      3)  ;; Cross shape
(define MOUSE-CURSOR-POINTING_HAND  4)  ;; Pointing hand cursor
(define MOUSE-CURSOR-RESIZE-EW      5)  ;; Horizontal resize/move arrow shape
(define MOUSE-CURSOR-RESIZE-NS      6)  ;; Vertical resize/move arrow shape
(define MOUSE-CURSOR-RESIZE-NWSE    7)  ;; Top-left to bottom-right diagonal resize/move arrow shape
(define MOUSE-CURSOR-RESIZE-NESW    8)  ;; The top-right to bottom-left diagonal resize/move arrow shape
(define MOUSE-CURSOR-RESIZE-ALL     9)  ;; The omni-directional resize/move cursor shape
(define MOUSE-CURSOR-NOT-ALLOWED   10)  ;; The operation-not-allowed shape

;; TRACELOG 
(define LOG-ALL     0) ;; Display all logs
(define LOG-TRACE   1) ;; Trace logging, intended for internal use only
(define LOG-DEBUG   2) ;; Debug logging, used for internal debugging, it should be disabled on release builds
(define LOG-INFO    3) ;; Info logging, used for program execution info
(define LOG-WARNING 4) ;; Warning logging, used on recoverable failures
(define LOG-ERROR   5) ;; Error logging, used on unrecoverable failures
(define LOG-FATAL   6) ;; Fatal logging, used to abort program: exit(EXIT_FAILURE)
(define LOG-NONE    7) ;; Disable logging


;; Pixel formats
(define PIXELFORMAT-UNCOMPRESSED-GRAYSCALE     1 ) ;; 8 bit per pixel (no alpha)
(define PIXELFORMAT-UNCOMPRESSED-GRAY-ALPHA    2 ) ;; 8*2 bpp (2 channels)
(define PIXELFORMAT-UNCOMPRESSED-R5G6B5        3 ) ;; 16 bpp
(define PIXELFORMAT-UNCOMPRESSED-R8G8B8        4 ) ;; 24 bpp
(define PIXELFORMAT-UNCOMPRESSED-R5G5B5A1      5 ) ;; 16 bpp (1 bit alpha)
(define PIXELFORMAT-UNCOMPRESSED-R4G4B4A4      6 ) ;; 16 bpp (4 bit alpha)
(define PIXELFORMAT-UNCOMPRESSED-R8G8B8A8      7 ) ;; 32 bpp
(define PIXELFORMAT-UNCOMPRESSED-R32           8 ) ;; 32 bpp (1 channel - float)
(define PIXELFORMAT-UNCOMPRESSED-R32G32B32     9 ) ;; 32*3 bpp (3 channels - float)
(define PIXELFORMAT-UNCOMPRESSED-R32G32B32A32  10) ;; 32*4 bpp (4 channels - float)
(define PIXELFORMAT-COMPRESSED-DXT1-RGB        11) ;; 4 bpp (no alpha)
(define PIXELFORMAT-COMPRESSED-DXT1-RGBA       12) ;; 4 bpp (1 bit alpha)
(define PIXELFORMAT-COMPRESSED-DXT3-RGBA       13) ;; 8 bpp
(define PIXELFORMAT-COMPRESSED-DXT5-RGBA       14) ;; 8 bpp
(define PIXELFORMAT-COMPRESSED-ETC1-RGB        15) ;; 4 bpp
(define PIXELFORMAT-COMPRESSED-ETC2-RGB        16) ;; 4 bpp
(define PIXELFORMAT-COMPRESSED-ETC2-EAC-RGBA   17) ;; 8 bpp
(define PIXELFORMAT-COMPRESSED-PVRT-RGB        18) ;; 4 bpp
(define PIXELFORMAT-COMPRESSED-PVRT-RGBA       19) ;; 4 bpp
(define PIXELFORMAT-COMPRESSED-ASTC-4x4-RGBA   20) ;; 8 bpp
(define PIXELFORMAT-COMPRESSED-ASTC-8x8-RGBA   21) ;; 2 bpp
