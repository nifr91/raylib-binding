#!/usr/bin/env -S guile  -L .  -L lib --r7rs -s 
!#

(import 
  (scheme base)
  (scheme process-context)
  (ice-9 format) 
  (raylib)) 

(set-trace-log-level LOG-NONE)
(define (main args)
  (define sw 800)
  (define sh 400)
  (define box-pos-y (- (/ sh 2) 40))
  (define scroll-speed 4)

  (init-window sw sh "input mouse wheel")
  (set-target-fps 60)
  (let main-loop () 
    (if (window-should-close)
      (close-window)
      (begin
        (set! box-pos-y (- box-pos-y (* (get-mouse-wheel-move) scroll-speed)))
        (begin-drawing)
        (clear-background BLACK)
        (draw-rectangle (- (/ sw 2) 40) (exact (round box-pos-y)) 80 80 MAROON)
        (draw-text "Use mouse wheel to move the cube up and down!" 10 10 20 LIGHTGRAY)
        (draw-text 
          (format #f "Box position Y: ~3,'0d" (exact box-pos-y)) 10 30 20 LIGHTGRAY)
        (end-drawing)
        (main-loop)))))

(main (cdr (command-line)))
