#!/usr/bin/env -S guile -L . -L lib --r7rs -s 
!#

(import 
  (scheme base) 
  (scheme process-context)
  (raylib))

(set-trace-log-level LOG-NONE) 

(define (main args) 
  (define screen-width  800)
  (define screen-height 450)
  (define ball-position (vector (/ screen-width 2.0) (/ screen-height 2.0)))
     
  (init-window screen-width screen-height "keyboard input")
  (set-target-fps 60)
  
  (let loop () 
    (if (window-should-close)
      (close-window)
      (begin 
        (if (is-key-down KEY-RIGHT) (vector-set! ball-position 0 (+ (vector-ref ball-position 0) 2.0)))
        (if (is-key-down KEY-LEFT)  (vector-set! ball-position 0 (- (vector-ref ball-position 0) 2.0)))
        (if (is-key-down KEY-UP)    (vector-set! ball-position 1 (- (vector-ref ball-position 1) 2.0)))
        (if (is-key-down KEY-DOWN)  (vector-set! ball-position 1 (+ (vector-ref ball-position 1) 2.0)))
        (begin-drawing)
        (clear-background BLACK)
        (draw-text "move the ball with arrow keys" 10 10 20 GRAY)
        (draw-circle-v ball-position 50.0 MAROON)
        (end-drawing)

        (loop))
      )))

(main (cdr (command-line)))
