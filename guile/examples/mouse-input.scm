#!/usr/bin/env -S guile -L . -L lib --r7rs -s 
!#

(import 
  (scheme base) 
  (scheme process-context)
  (raylib))

(set-trace-log-level LOG-NONE)
(define (main args) 
  (define sw 800)
  (define sh 400)
  (define bpos #(-100 -100))
  (define bcolor PURPLE)

  (init-window sw sh "mouse input")
  (set-target-fps 60)

  (let loop () 
    (if (window-should-close)
      (close-window)
      (begin 
        (set! bpos (get-mouse-position))
        (cond 
          ((is-mouse-button-released MOUSE-BUTTON-LEFT)   (set! bcolor MAROON))
          ((is-mouse-button-released MOUSE-BUTTON-MIDDLE) (set! bcolor LIME))
          ((is-mouse-button-released MOUSE-BUTTON-RIGHT)  (set! bcolor DARKBLUE))
          ((is-mouse-button-released MOUSE-BUTTON-EXTRA)  (set! bcolor ORANGE))
          ((is-mouse-button-released MOUSE-BUTTON-SIDE)   (set! bcolor GRAY)))
        (begin-drawing)
        (clear-background BLACK)
        (draw-circle-v (get-mouse-position) 50 bcolor)
        (draw-text "move ball with mouse and click mouse button to change color" 10 10 20 GRAY)
        (end-drawing)
        (loop)))))

(main (cdr (command-line)))
