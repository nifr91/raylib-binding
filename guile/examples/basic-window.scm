#!/usr/bin/env -S guile -L . -L lib --r7rs -s 
!#

(import 
  (scheme base) 
  (scheme process-context)
  (raylib))

(define (main args) 
  (set-trace-log-level LOG-NONE)
  (init-window 800 400 "basic-window")
  (set-target-fps 60)
  (let loop () 
    (if (eq? #f (window-should-close)) 
      (begin 
        (begin-drawing)
        (clear-background BLACK)
        (draw-text "Congrats! You created your fist window"
          190 200 20 WHITE)
        (do ((i 0 (+ i 1))) ((>= i 500))
          (draw-pixel (+ 150 i) 220 GRAY))
        (end-drawing)
        (loop))
      (close-window))))

(main (cdr (command-line)))
