#!/usr/bin/env -S guile -L . -L lib --r7rs -s 
!#

(import 
  (scheme base)
  (raylib))

;; Initialization 
;; ---------------------------------------------------------------------------
(set-trace-log-level LOG-NONE)
(define w 1000)
(define h 1000)
(init-window w h "raylib [textures] example - image drawing")

;; Load Image  in CPU memory (RAM)
(define img (load-image "raylib-logo.png"))
;; Generate Image of solid color 
(define rect (gen-image-color w h GRAY))

;; NOTE: Textures MUST be loaded after Window initialization (OpenGL context is 
;; required)

;; Image converted to texture, uploaded to GPU memory (VRAM)
(define txt (load-texture-from-image img))
(define rect-tx #f)

;; Once image has been converted to texture and uploaded to VRAM, it can be 
;; unloaded from RAM
(unload-image img)

(set-target-fps 30)

;; Main gaim loop 
(let loop ()
  ;; Detect if window close button or ESC key is pressed
  (if (window-should-close)
    (begin 
      ;; De-initialization 
      (unload-texture txt)
      (close-window))
    (begin
      ;; Update 
      ;; ---------------------------------------------------------------------
      ;; TODO :: Uupdate
      ;; ---------------------------------------------------------------------
      (set! rect-tx (load-texture-from-image rect))
      
      ;; Draw
      ;; ---------------------------------------------------------------------
      (begin-drawing)
      (clear-background BLACK)

      (draw-texture rect-tx 0 0 WHITE)
      (draw-texture txt 
        (- (/ w 2) (/ (image-width img) 2))  
        (- (/ h 2) (/ (image-height img) 2))
        WHITE) 
       
      (draw-fps 0 0)
      (end-drawing)
      ;; ---------------------------------------------------------------------
      (unload-texture rect-tx)
      (loop))))
