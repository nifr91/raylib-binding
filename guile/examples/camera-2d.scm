#!/usr/bin/env -S guile --r7rs -L . -L lib -s 
!#

(import 
  (scheme base)
  (scheme process-context)
  (srfi srfi-27)
  (raylib))

(random-source-randomize! default-random-source)
(define (rand-int l r)
  (+ l (random-integer (- r l))))

(define MAX-BUILDINGS 100)
(set-trace-log-level LOG-NONE)
(define (main args)
  (define sw 800)
  (define sh 450)
  (define player (make-rectangle 400 280 40 40))
  (define buildings (make-vector MAX-BUILDINGS))
  (define build-colors (make-vector MAX-BUILDINGS))
  (define spacing 0)
  (define camera (make-camera2d (vector 0 0) (vector 0 0) 0 0))
  

  ;; populate building and color arrays 
  (do ((i 0 (+ i 1))) ((>= i MAX-BUILDINGS))
    (vector-set! buildings i
      (let*
        ((bw (inexact (rand-int 50 200)))
        (bh (inexact (rand-int 100 800)))
        (y (- sh 130.0 bh))
        (x (- spacing 6000.0)))
        (set! spacing (+ spacing bw))
        (make-rectangle x y bw bh)))
    (vector-set! build-colors i
      (make-color (rand-int 200 240) (rand-int 200 240) (rand-int 200 250) 255)))

  (set! camera 
    (make-camera2d
    (vector (/ sw 2.0) (/ sh 2.0)) 
    (vector (+ (rectangle-x player) 20.0) (+ (rectangle-y player) 20.0))
    0.0 1.0))

  (init-window sw sh "2d camera")
  (set-target-fps 60)
  (let main-loop () 
    ;; update ---------------------------

    ;; player movement 
    (if (is-key-down KEY-RIGHT) (rectangle-x! player (+ (rectangle-x player) 2) ))
    (if (is-key-down KEY-LEFT)  (rectangle-x! player (- (rectangle-x player) 2) ))

    ;; camera folows player
    (camera2d-target! camera (vector  (+ (rectangle-x player) 20.0) (+ (rectangle-y player) 20.0)))

    ;; camera rotation controls
    (if (is-key-down KEY-A) (camera2d-rotation! camera (- (camera2d-rotation camera) 1)))
    (if (is-key-down KEY-S) (camera2d-rotation! camera (+ (camera2d-rotation camera) 1)))

    ;; limit camera rotation 
    (if (> (camera2d-rotation camera) 40)  (camera2d-rotation! camera 40.0))
    (if (< (camera2d-rotation camera) -40) (camera2d-rotation! camera -40.0))
  
    ;; camera zoom controls 
    (camera2d-zoom! camera (+ (camera2d-zoom camera) (* (get-mouse-wheel-move) 0.05)))
    (if (> (camera2d-zoom camera) 3.0) (camera2d-zoom! camera 3.0))
    (if (< (camera2d-zoom camera) 0.1) (camera2d-zoom! camera 0.1))

    ;; reset (zoom and rotation)
    (if (is-key-pressed KEY-R) (begin
      (camera2d-zoom! camera 1.0)
      (camera2d-rotation! camera 0.0)))

    (if (window-should-close) 
      (close-window)
      (begin
        (begin-drawing)
          (begin-mode-2d camera)
            (clear-background BLACK)
            (draw-rectangle -6000 320 13000 8000 DARKGRAY)
            (do ((i 0 (+ i 1))) ((>= i MAX-BUILDINGS))
              (draw-rectangle-rec (vector-ref buildings i) (vector-ref build-colors i)))
              (draw-rectangle-rec player RED)
          (end-mode-2d)

        (draw-text "SCREEN AREA" 640 10 20 RED)
        (draw-rectangle 0 0 sw 5 RED)
        (draw-rectangle 0 5  5 (- sh 10) RED)
        (draw-rectangle (- sw 5) 5 5 (- sh 10) RED)
        (draw-rectangle 0 (- sh 5) sw 5 RED)
        
        (draw-rectangle 10 10 250 113 (fade SKYBLUE 0.8))
        (draw-rectangle-lines 10 10 250 113 BLUE)

        (draw-text "free 2d camera controls:" 20 20 10 BLACK)
        (draw-text "- right/left to move offset" 40 40 10 DARKGRAY)
        (draw-text "- mouse wheel to zoom in-out" 40 60 10 DARKGRAY)
        (draw-text "- a/s to rotate" 40 80 10 DARKGRAY)
        (draw-text "- r to reset zoom and rotation" 40 100 10 DARKGRAY)

        (end-drawing)
        (main-loop)))))

(main (cdr (command-line)))

  

